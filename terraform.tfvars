# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

# Network information - provided via agentk-network
subnet = "agentk-sculpin-subnet"
vpc = "agentk-sculpin-vpc"

# Service Account Information - provided via agentk-service-account
cluster_sa_email = "agentk-guinea-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"
cluster_sa_id = "111443113326302267295"

# Naming prefix
prefix = "agentk"

