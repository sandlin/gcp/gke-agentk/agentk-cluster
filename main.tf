/*
* Create Network for our TF cluster.
*/
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/35455533/terraform/state/groot"
    lock_address   = "https://gitlab.com/api/v4/projects/35455533/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/35455533/terraform/state/groot/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

provider "google" {
  project     = var.project
}

resource "random_pet" "name" {
  length = 1
}

locals {
  cluster_name = "${var.prefix}-${random_pet.name.id}"
}
/*
* Create GKE Cluster & grant bindings required for internal management.
*/

# GKE cluster
resource "google_container_cluster" "primary" {
  name     = local.cluster_name
  location = var.region
  
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = var.vpc
  subnetwork = var.subnet
  
  master_auth {
    client_certificate_config {
      issue_client_certificate = true
    }
  }
}

# 2021-12-22T13:17:11.264-0800 [WARN]  Provider "provider[\"registry.terraform.io/hashicorp/google\"]" produced an unexpected new value for google_container_node_pool.primary_nodes, but we are tolerating it because it is using the legacy plugin SDK.
#     The following problems may be the cause of any confusing errors from downstream operations:
#       - .node_config[0].min_cpu_platform: was null, but now cty.StringVal("")
#       - .node_config[0].node_group: was null, but now cty.StringVal("")
      # - .subject[0].namespace: planned value cty.StringVal("default") for a non-computed attribute
      # - .subject[1].namespace: planned value cty.StringVal("default") for a non-computed attribute
# 2021-12-22T13:17:11.264-0800 [TRACE] NodeAbstractResouceInstance.writeResourceInstanceState to workingState for google_container_node_pool.

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = "${local.cluster_name}-np"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = var.cluster_sa_email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project
    }

    preemptible  = true
    machine_type = "n1-standard-1"
    tags         = ["${local.cluster_name}-node", "${var.project}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}